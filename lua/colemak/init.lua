local function map(mode, lhs, rhs, opts)
    local options = { noremap = true, silent = true }
    if opts then
      options = vim.tbl_extend("force", options, opts)
    end
    vim.api.nvim_set_keymap(mode, lhs, rhs, options)
  end
  
-- Vim for Colemak
map("", "n", "j", {})
map("", "e", "k", {})
map("", "i", "l", {})
map("", "j", "e", {})
map("", "k", "n", {})
map("", "l", "i", {})
map("", "K", "N", {})
map("", "L", "I", {})
map("", "N", "5j", {})
map("", "E", "5k", {})
  
map("", "H", "0", {})
map("", "I", "$", {})

map("", "<c-s>", ":w<cr>", {})


-- Copy and Past from the system clipboard
map("v", "Y", '"+y', {})
map("n", "P", '"+p', {})

map("n", "<space>", "viw", {}) -- select a word
